package com.example.controlleradvice.config;

import com.example.controlleradvice.service.IndexService;
import com.example.controlleradvice.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
@RequiredArgsConstructor
public class GlobalControllerAdvice {

    private final UserService userService;
    private final IndexService indexService;

    @ExceptionHandler({Exception.class})
    public ModelAndView globalException(final Exception ex, final HttpServletRequest request) {
     //   userService.getActiveUser().ifPresent(System.out::print);
        return new ModelAndView("index")
                .addObject("viewModel", indexService.getIndexViewModelWithErrorMessage(ex));
    }
}