package com.example.controlleradvice.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;
import org.springframework.web.servlet.resource.GzipResourceResolver;
import org.springframework.web.servlet.resource.PathResourceResolver;

import javax.annotation.PostConstruct;
import java.nio.file.Path;

@Configuration
@EnableWebMvc
public class MvcConfig extends WebMvcConfigurerAdapter {

    private static final String[] DEFAULT_STATIC_RESOURCES = {
            "classpath:/META-INF/resources/", "classpath:/resources/",
            "classpath:/static/", "classpath:/public/"
    };
    @Getter
    private static Path documentPath;
    @Getter
    private static Path originalImagePath;
    @Getter
    private static Path resourceImagePath;
    @Getter
    private static Path temporaryPath;
    @Getter
    private static String documentWebUrl;
    @Getter
    private static String imageWebUrl;
    @Getter
    private static String imageOriginalUrl;


    @Autowired
    private RequestMappingHandlerAdapter requestMappingHandlerAdapter;

    @PostConstruct
    public void init() {
        requestMappingHandlerAdapter.setIgnoreDefaultModelOnRedirect(true);
    }

    @Override
    public void addResourceHandlers(final ResourceHandlerRegistry registry) {

        registry.addResourceHandler(getDocumentWebUrl() + "/**")
                .addResourceLocations("file:" + getDocumentPath() + "/")
                .setCachePeriod(3600)
                .resourceChain(true)
                .addResolver(new GzipResourceResolver())
                .addResolver(new PathResourceResolver());
        registry.addResourceHandler(getImageWebUrl() + "/**")
                .addResourceLocations("file:" + getResourceImagePath() + "/")
                .setCachePeriod(3600)
                .resourceChain(true)
                .addResolver(new GzipResourceResolver())
                .addResolver(new PathResourceResolver());
        registry.addResourceHandler(getImageOriginalUrl() + "/**")
                .addResourceLocations("file:" + getOriginalImagePath() + "/")
                .setCachePeriod(3600)
                .resourceChain(true)
                .addResolver(new GzipResourceResolver())
                .addResolver(new PathResourceResolver());
        registry.addResourceHandler("/**")
                .addResourceLocations(DEFAULT_STATIC_RESOURCES)
                .setCachePeriod(3600)
                .resourceChain(true)
                .addResolver(new GzipResourceResolver())
                .addResolver(new PathResourceResolver());
    }

}
