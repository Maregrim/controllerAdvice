package com.example.controlleradvice.viewmodel;

import com.example.controlleradvice.model.Location;
import lombok.Value;

import java.util.List;


@Value
public class IndexViewModel {

    private List<Location> locations;
    private String errorMessage;

    public boolean hasErrorMessage() {
        return errorMessage != null;
    }
}
