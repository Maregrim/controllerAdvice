package com.example.controlleradvice.service;

import com.example.controlleradvice.model.Location;
import com.example.controlleradvice.repository.LocationRepository;
import com.example.controlleradvice.viewmodel.IndexViewModel;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Transactional
public class IndexService {

    private final LocationRepository locationRepository;
    //private final UserService userService;

    public IndexViewModel getIndexViewModel() {
        return new IndexViewModel(locationRepository.findAll(), null);
    }

    public IndexViewModel getIndexViewModel(final Location location) {
        final List<Location> locations = new ArrayList<>();
        locations.add(location);
        return new IndexViewModel(locations, null);
    }

    public IndexViewModel getIndexViewModelWithErrorMessage(final Exception ex) {
        return new IndexViewModel(locationRepository.findAll(), ex.getMessage());
    }
}