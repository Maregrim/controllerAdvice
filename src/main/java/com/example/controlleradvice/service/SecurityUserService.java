package com.example.controlleradvice.service;

import com.example.controlleradvice.model.User;
import com.example.controlleradvice.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class SecurityUserService implements UserDetailsService {

    private final UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        final Optional<User> user =
                userRepository.findByUsername(username);
        if (!user.isPresent()) {
            throw new UsernameNotFoundException(username + " is not present.");
        }
        return user.get();
    }
}