package com.example.controlleradvice.controller;

import com.example.controlleradvice.model.Location;
import com.example.controlleradvice.service.IndexService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequiredArgsConstructor
@RequestMapping("show")
public class ShowController {

    private final IndexService indexService;

    @GetMapping
    public String showWithId(final @RequestParam Location location, final Model model) {
        model.addAttribute("viewModel", indexService.getIndexViewModel(location));
        return "index";
    }
}
