package com.example.controlleradvice.controller;

import com.example.controlleradvice.service.IndexService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequiredArgsConstructor
@RequestMapping
public class IndexController {

    private final IndexService indexService;

    @GetMapping
    public String index(final Model model) {
        model.addAttribute("viewModel", indexService.getIndexViewModel());
        return "index";
    }
}
