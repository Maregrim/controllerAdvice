package com.example.controlleradvice.repository;


import com.example.controlleradvice.model.Location;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocationRepository extends JpaRepository<Location, Long> {


}
