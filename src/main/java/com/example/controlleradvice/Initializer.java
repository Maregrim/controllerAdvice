package com.example.controlleradvice;

import com.example.controlleradvice.model.Location;
import com.example.controlleradvice.model.User;
import com.example.controlleradvice.repository.LocationRepository;
import com.example.controlleradvice.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
@RequiredArgsConstructor
public class Initializer implements CommandLineRunner {

    private final UserRepository userRepository;
    private final LocationRepository locationRepository;

    @Override
    public void run(String... args) throws Exception {
        if (!userRepository.findByUsername("user").isPresent()) {
        userRepository.save(new User().setEmail("blabla@bla.de").setUsername("user").setPassword("user"));
        ArrayList<Location> locations = new ArrayList<>();
        locations.add(new Location().setName("Kölner Dom").setLocation("Köln"));
        locations.add(new Location().setName("birmingham palace").setLocation("London"));
        locations.add(new Location().setName("tour eiffel").setLocation("Paris"));
        locationRepository.save(locations);
        }
    }
}
