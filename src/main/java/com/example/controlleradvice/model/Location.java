package com.example.controlleradvice.model;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;

@Entity
@ToString
@EqualsAndHashCode
public class Location implements Serializable {

    @Getter
    @Id
    @GeneratedValue
    private Long id;
    @Getter @Setter
    private String name;
    @Getter @Setter
    private String location;

}
